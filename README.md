Run your Webhook API and Run `ngrok` for forward the `localhost` to `domain` and copy the `http://your_forwarding_domain/your_hook_url` to URL feild on `Pmoney` app.

### 1. [Manually Install](https://ngrok.com/download)
### 2. CMD Install

#### Install `ngrok` on MasOS
```
brew install ngrok/ngrok/ngrok
```

#### Install `ngrok` on Windows
```
choco install ngrok
```
### 3. Run `ngrok`

```
ngrok http <port>
```