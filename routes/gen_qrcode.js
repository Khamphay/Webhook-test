/*!
 * promptpay-qr
 * JavaScript library to generate PromptPay QR code
 * <https://github.com/dtinth/promptpay-qr>
 *
 * Refs:
 * - https://www.blognone.com/node/95133
 * - https://www.emvco.com/emv-technologies/qrcodes/
 *
 * @license MIT
 */

const { crc16xmodem } = require("crc");

function generatePayload(target, options) {
  const date = new Date(Date.now());
  const result = Math.random().toString(36).substring(4, 8);
  const amount = options.amount ? options.amount.toString() : null;
  const qr_expire_time = new Date(date)
    .setMinutes(date.getMinutes() + 2)
    .toString();
  // const secretkey = "8aa2f3d8-ffa1-430c-9beb-3c77446b2907";
  const Billid = `B${Date.now()}${result}`.toLocaleLowerCase();
  const aid = "A005266284662577";
  const iin = "12345678";
  var receiver_id = target;

  var data = [
    f("00", "01"),
    f("01", amount ? "12" : "11"),
    f(
      "38",
      serialize([
        f("00", aid),
        f("01", iin),
        f("02", "002"),
        f("03", receiver_id),
        qr_expire_time && f("04", qr_expire_time),
      ])
    ),
    options.mccCode && f("52", options.mccCode), // mcc
    f("53", "418"),
    amount && f("54", amount),
    Billid && f("62", serialize([f("01", Billid)])),

    f("58", "LA"),
  ];
  var dataToCrc = serialize(data) + "63" + "04";
  data.push(f("63", formatCrc(crc16xmodem(dataToCrc, 0xffff))));
  return serialize(data);
}

function f(id, value) {
  return [id, ("00" + value.length).slice(-2), value].join("");
}

function serialize(xs) {
  return xs
    .filter(function (x) {
      return x;
    })
    .join("");
}

function sanitizeTarget(id) {
  return id.replace(/[^0-9]/g, "");
}

function formatTarget(id) {
  const numbers = sanitizeTarget(id);
  if (numbers.length >= 13) return numbers;
  return ("0000000000000" + numbers.replace(/^0/, "66")).slice(-13);
}

function formatAmount(amount) {
  return amount.toFixed(2);
}

function formatCrc(crcValue) {
  return ("0000" + crcValue.toString(16).toUpperCase()).slice(-4);
}

module.exports = { generatePayload };
